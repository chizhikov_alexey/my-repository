#include <iostream>
#include <cstdlib>
#include <ctime>
#include <cstring>
#include <cmath>
#include <cfloat>

using namespace std;

unsigned long long addNum = 0;
unsigned long long mulNum = 0;

double fRand(double fMin, double fMax) {
    double f = (double) rand() / RAND_MAX;
    return fMin + f * (fMax - fMin);
}

class Matrix {
    double **elem;
    long long rows, cols;
    bool kill;


public:

    Matrix(long long rows, long long cols) :
            rows(rows), cols(cols), kill(true) {
        elem = new double *[rows];
        for (int i = 0; i < rows; ++i) {
            elem[i] = new double[cols];
        }
    }

    Matrix(long long rows, long long cols, double **elem) :
            rows(rows), cols(cols), elem(elem), kill(true) {}

    void set(double **elem) {
        for (int i = 0; i < rows; ++i) {
            delete[] elem[i];
        }
        delete[] elem;

        this->elem = elem;
    }

    double *operator[](int num) {
        return elem[num];
    }

    void randMatrix() {
        for (int i = 0; i < rows; ++i) {
            for (int j = 0; j < cols; ++j) {
                elem[i][j] = fRand(-1, 1);
            }
        }
    }

    void printMatrix() {
        for (int i = 0; i < rows; ++i) {
            for (int j = 0; j < cols; ++j) {
                cout.precision(8);
                cout.width(12);
                cout << elem[i][j];
            }
            cout << endl;
        }
    }

    pair<Matrix &, Matrix &> GS() {
        Matrix *q = new Matrix(rows, cols);
        Matrix *r = new Matrix(cols, cols);
        Matrix &Q = *q;
        Matrix &R = *r;

        for (long long j = 0; j < cols; j++) {
            
            //Qj = Aj
            for (long long k = 0; k < rows; k++) {
                Q[k][j] = elem[k][j];
            }

            for (long long i = 0; i < j; i++) {
                //Rij =QiTAj
                for (long long k = 0; k < rows; k++) {
                    
                    R[i][j] += Q[k][i] * elem[k][j];

                    mulNum++;
                    addNum++;
                }

                //Qj = Qj −RijQi                
                for (long long k = 0; k < rows; k++) {
                Q[k][j] -= R[i][j] * Q[k][i];
                    mulNum++;
                    addNum++;
                }
                
            }
            
            R[j][j] = Q.colNorm(j);
            for (long long k = 0; k < rows; k++) {
                //Qj = Qj / Rjj
                Q[k][j] /= R[j][j];
                mulNum++;
            }

            //Checks ||Qj|| ~ 1
            if (fabs(Q.colNorm(j) - 1.0) > 10e-12) {
                j--;
            }
        }

        return pair<Matrix &, Matrix &>(Q, R);
    }

    double norm() {
        double norm = 0;
        for (long long i = 0; i < rows; i++) {
            for (long long j = 0; j < cols; j++) {
                norm += elem[i][j] * elem[i][j];
            }
        }
        norm = sqrt(norm);
        return norm;
    }

    double colNorm(long long col) {
        double norm = 0;
        for (long long i = 0; i < rows; i++) {
            norm += elem[i][col] * elem[i][col];
        }
        norm = sqrt(norm);
        return norm;
    }


    ~Matrix() {
        if (kill) {
            for (int i = 0; i < rows; ++i) {
                delete[] elem[i];
            }
            delete[] elem;
        }
    }
};

int main(int argc, char const *argv[]) {
    srand(time(NULL));
    long long rows = atol(argv[1]);
    long long cols = atol(argv[2]);
    Matrix my_matrix (rows, cols);
    my_matrix.randMatrix();
    
    pair < Matrix & , Matrix &> QR = my_matrix.GS();

    //QR
    Matrix result (cols, rows);
    for (long long i = 0; i < rows; i++) {
    	for (long long j = 0; j < cols; j++) {
    		for (long long k = 0; k < cols; k++) {
    			result[i][j] += QR.first[i][k] * QR.second[k][j];
    		}
    	}
    }

    //A - QR
    Matrix diff (cols, rows);
    for (long long i = 0; i < rows; i++) {
        for (long long j = 0; j < cols; j++) {
            diff[i][j] = my_matrix[i][j] - result[i][j];
        }
    }


    if (argc >= 4 && strncmp(argv[3], "-p", 2) == 0) {
        cout << "Random matrix:" << endl;
        my_matrix.printMatrix();
        cout << endl << "Q =" << endl;
        QR.first.printMatrix();
        cout << endl << "R =" << endl;
        QR.second.printMatrix();
        cout << endl << "Q * R =" << endl;
        result.printMatrix();
        cout << endl;
    }
    
    
    double Anorm = my_matrix.norm();
    double QRnorm = result.norm();
    double diffNorm = diff.norm();
    cout << "||A|| = " << Anorm << endl;
    cout << "||A - QR|| / ||A|| = " << diffNorm / Anorm << endl;
    cout << endl << "Additions: " << addNum << endl << "Multiplications: " << mulNum << endl;

    delete &QR.first;
    delete &QR.second;
    return 0;
}